* Picture This.
One of the creative coding works by deconbatch.
[[https://www.deconbatch.com/2020/11/picture-this-vector-field-method-to.html]['Picture This.' on Deconbatch's Creative Coding : Examples with Code.]]

[[./example01.png][An example image of the image processing with Vector Field.]]

** This creative coding process the image hard!
   - This is the creative coding of image processing. It was written in Processing.
   - It's a near relation work of [[https://www.deconbatch.com/2019/02/daydream.html]['Daydream']]. It uses the Vector Field method to process the image. It starts the Vector Field from the points of edges in the image.
   - You can use your image.
     :   PImage img = loadImage("your_photo.png");
   - One of the keys of this code is using nested 3D noise to calculate the Vector Field.
     : noise(xPrev * _noiseDiv, yPrev * _noiseDiv, noise(yPrev * _noiseDiv * 3.0) * _uneven)
     - Ordinary, I use 2D noise like 'noise(x, y)'.
       - [[example11.png][An example image of 2D noise.]]
     - And I found the interesting effect when I used nested 3D noise like 'noise(x, y, noise(x, y))'.
       - [[example12.png][An example image of nested 3D noise.]]
   - And the other key of this code is limiting the direction of the path of the Vector Field.
     : cos(TWO_PI * round(nX * _curlMult * _angles) / _angles)
     - [[example02.png][An example image : no limitting.]]
     - [[example03.png][An example image : limitting the direction.]]
** Change log.
   - created : 2020/11/19
